CREATE TABLE sdgt_accounts (
	pk serial primary key,
	user_name text not null,
	password text not null,
    user_details jsonb,
	date_created timestamp with time zone DEFAULT now(),
	archived boolean DEFAULT false
);
ALTER TABLE sdgt_accounts OWNER TO isad2;

alter table isad_accounts add failed_login_counter integer;