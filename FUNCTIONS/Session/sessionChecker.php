<?php
$_id = md5('pk');
if (isset($_COOKIE[$_id]) && !empty($_COOKIE[$_id])) {
    setcookie($_id, $_COOKIE[$_id], time() + 7200000, '/');
    header("HTTP/1.0 200 Have Active Session");
    return 'Have Active Session';
} else {
    header("HTTP/1.0 404 No Active Session");
    return 'No Session Active';
}
