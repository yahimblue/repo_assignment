<?php
require_once '../sdgt_connect.php';
require_once '../../CLASSES/Sessions.php';

$data = array();
foreach ($_POST as $k => $v) {
    $data[$k] = $v;
}

$class = new Sessions($data);
$data = $class->updatingAccountsStatus($data);

header("HTTP/1.0 Internal Server Error");
if ($data['status'] == true) {
    header("HTTP/1.0 200 OK");
}

header('Content-Type: application/json');
print(json_encode($data));
?>