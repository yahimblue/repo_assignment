<?php
require_once '../sdgt_connect.php';
require_once '../../CLASSES/Sessions.php';
$data = array();
foreach ($_POST as $k => $v) {
    $data[$k] = $v;
}
$class = new Sessions($data);
$data = $class->auth($data);
if ($data['status'] == true) {
    $pk = md5('pk');
    setcookie($pk, md5($data['result'][0]['pk']), time() + 7200000, '/');
    header("HTTP/1.0 200 OK");
} else {
    header("HTTP/1.0 404 No Data Found");
}
header('Content-Type: application/json');
print(json_encode($data));
