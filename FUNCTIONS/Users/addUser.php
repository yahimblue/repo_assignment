<?php
require_once '../sdgt_connect.php';
require_once '../../CLASSES/Users.php';
$data = array();
foreach ($_POST as $k => $v) {
    $data[$k] = $v;
}
function generateRandomString()
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';

    for ($i = 0; $i < 12; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
$data['code'] = generateRandomString();
if ($data['final_password'] == 'default pass') {
    $data['final_password'] = 'comptool123456';
}
$class = new Users($data);
$data = $class->addUser($data);
if ($data['status'] == true) {
    header("HTTP/1.0 200 OK");
} else {
    header("HTTP/1.0 500 Internal Server Error");
}
header('Content-Type: application/json');
print(json_encode($data));
