<?php
require_once '../sdgt_connect.php';
require_once '../../CLASSES/Users.php';
$data = array();
foreach ($_POST as $k => $v) {
    $data[$k] = $v;
}

$class = new Users($data);
$data = $class->updateUser($data);

if ($data['status'] == true) {
    header("HTTP/1.0 200 OK");
} else {
    header("HTTP/1.0 500 Internal Server Error");
}

header('Content-Type: application/json');
print(json_encode($data));
