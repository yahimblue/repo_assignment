<?php
require_once '../sdgt_connect.php';
require_once '../../CLASSES/Users.php';
$data = array();
foreach ($_POST as $k => $v) {
    $data[$k] = $v;
}
$class = new Users($data);
$data = $class->getUsers($data);
if ($data['status'] == true) {
    header("HTTP/1.0 200 OK");
} else {
    header("HTTP/1.0 404 No Data Found");
}

header('Content-Type: application/json');
print(json_encode($data));
