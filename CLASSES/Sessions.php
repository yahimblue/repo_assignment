<?php
require_once '../../CLASSES/ClassParent.php';
class Sessions extends ClassParent
{

    private $options = [
        'salt' => '$6$rounds=5000$ComparativeAnalyticsToolPBE2019$',
        'cost' => 10,
    ];

    public function auth($data)
    {
        foreach ($data as $k => $v) {
            $data[$k] = pg_escape_string(trim(strip_tags($v)));
        }

        $user_name = $data['user_name'];
        $unstripped_pass = password_hash($data['password'], PASSWORD_BCRYPT, $this->options);
        $password = $unstripped_pass;

        $sql = <<<EOT
            SELECT * FROM isad_accounts
            WHERE user_name = '$user_name'
            AND password = '$password'
            AND archived = 'f'
            ;
EOT;
        return ClassParent::get($sql);
    }

    public function get_profile($data)
    {
        foreach ($data as $k => $v) {
            $data[$k] = pg_escape_string(trim(strip_tags($v)));
        }

        $pk = $data['pk'];

        $sql = <<<EOT
            SELECT
                pk,
                user_name,
                user_details
            FROM isad_accounts
            WHERE archived = 'f'
            AND md5(pk::text) = '$pk'
            ;
EOT;
        return ClassParent::get($sql);
    }

    public function update_password_reset_status($data)
    {
        foreach ($data as $k => $v) {
            $data[$k] = pg_escape_string(trim(strip_tags($v)));
        }

        $pk = $data['pk'];

        $sql = <<<EOT
           UPDATE isad_accounts SET passwordreset = 'f'
           WHERE pk = '$pk' AND archived = 'f'
           ;
EOT;
        return ClassParent::update($sql);
    }

    public function add_security($data)
    {
        foreach ($data as $k => $v) {
            $data[$k] = pg_escape_string(trim(strip_tags($v)));
        }

        $pk = $data['pk'];

        $code = password_hash($data['code'], PASSWORD_BCRYPT, $this->options);

        $sql = <<<EOT
            UPDATE isad_accounts set security_code = '$code' where pk = '$pk';
EOT;
        return ClassParent::update($sql);
    }

    public function forgot_password($data)
    {
        foreach ($data as $k => $v) {
            $data[$k] = pg_escape_string(trim(strip_tags($v)));
        }

        $code = password_hash($data['code'], PASSWORD_BCRYPT, $this->options);

        $get_pk = <<<EOT
            SELECT pk from isad_accounts where security_code = '$code';
EOT;
        $gets_pk = ClassParent::get($get_pk);
        if ($gets_pk['result']) {
            $password = password_hash('dagat123456', PASSWORD_BCRYPT, $this->options);
            $pk = $gets_pk['result'][0]['pk'];
            $sql = <<<EOT
                UPDATE isad_accounts set password = '$password' where pk = '$pk';
EOT;
            return ClassParent::update($sql);
        }
    }

    public function getUserLogin($data)
    {
        foreach ($data as $k => $v) {
            $data[$k] = pg_escape_string(trim(strip_tags($v)));
        }

        $user_name = $data['user_name'];
        $failedCounter = $data['failedCounter'];

        $sql = "begin;";
        $userLoginDetails .= <<<EOT
            SELECT * from isad_accounts where user_name = '$user_name' AND archived = 'f'
            AND failed_login_counter != 0;
EOT;
        $getUserLogging = ClassParent::get($userLoginDetails);
        if ($getUserLogging['status'] == true) {
            $sql .= <<<EOT
            UPDATE isad_accounts set failed_login_counter = $failedCounter WHERE user_name = '$user_name';
EOT;
        } else {
            $sql .= <<<EOT
            UPDATE isad_accounts set archived = 't' WHERE user_name = '$user_name';
EOT;
        }
        $sql .= "commit;";
        return ClassParent::update($sql);
    }

    public function failedLoginCounterGetter($data)
    {
        foreach ($data as $k => $v) {
            $data[$k] = pg_escape_string(trim(strip_tags($v)));
        }

        $user_name = $data['user_name'];

        $sql = <<<EOT
           SELECT failed_login_counter FROM isad_accounts where user_name = '$user_name' AND archived = 'f';
EOT;
        return ClassParent::get($sql);
    }

    public function updatingAccountsStatus($data)
    {
        foreach ($data as $k => $v) {
            $data[$k] = pg_escape_string(trim(strip_tags($v)));
        }

        $sql = "begin;";
        $userLoginDetails .= <<<EOT
            SELECT pk from isad_accounts WHERE archived = 'f'
            AND failed_login_counter = 1;
EOT;
        $getUserLogging = ClassParent::get($userLoginDetails);
        if ($getUserLogging['status'] == true) {
            foreach ($getUserLogging['result'] as $k => $v) {
                $userPk = $v['pk'];
                $sql .= <<<EOT
                UPDATE isad_accounts set failed_login_counter = 5;
EOT;
            }
            $sql .= "commit;";
            return ClassParent::update($sql);
        }
    }
}
