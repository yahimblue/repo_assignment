<?php
require_once '../../CLASSES/ClassParent.php';
class Users extends ClassParent
{
    private $options = [
        'salt' => '$6$rounds=5000$ComparativeAnalyticsToolPBE2019$',
        'cost' => 10,
    ];

    public function addUser($data)
    {
        foreach ($data as $k => $v) {
            $data[$k] = pg_escape_string(trim(strip_tags($v)));
        }
        $user_name = $data['user_name'];
        $job_position = $data['job_position'];
        $full_name = $data['full_name'];

        $final_password_stripped = password_hash($data['final_password'], PASSWORD_BCRYPT, $this->options);
        $code = password_hash($data['code'], PASSWORD_BCRYPT, $this->options);
        $final_password = $final_password_stripped;

        $user_details_array = array(
            'full_name' => $full_name,
            'position' => $job_position,
        );
        $user_details = json_encode($user_details_array);

        $sql = <<<EOT
            INSERT INTO sdgt_accounts
            (
                user_name,
                password,
                user_details
            )
            VALUES
            (
                '$user_name',
                '$final_password',
                '$user_details'
            );
EOT;
        return ClassParent::insert($sql);
    }

    public function updateUser($data)
    {
        foreach ($data as $k => $v) {
            $data[$k] = pg_escape_string(trim(strip_tags($v)));
        }
        $pk = $data['pk'];
        $job_position = $data['job_position'];
        $full_name = $data['full_name'];
        $user_details_array = array(
            'full_name' => $full_name,
            'position' => $job_position,
        );
        $user_details = json_encode($user_details_array);

        $sql = <<<EOT
            UPDATE sdgt_accounts SET
            (
                user_details
            )
            =
            (
                '$user_details'
            )
            WHERE pk = $pk;
EOT;
        return ClassParent::update($sql);
    }

    public function updateUserAccountStatus($data)
    {
        foreach ($data as $k => $v) {
            $data[$k] = pg_escape_string(trim(strip_tags($v)));
        }
        $pk = $data['pk'];
        $status = $data['status'];

        $sql = <<<EOT
            UPDATE sdgt_accounts SET archived = '$status'
            WHERE pk = $pk;
EOT;
        return ClassParent::update($sql);
    }

    public function resetPassword($data)
    {
        foreach ($data as $k => $v) {
            $data[$k] = pg_escape_string(trim(strip_tags($v)));
        }
        $pk = $data['pk'];
        $password = 'comptool123456';
        $final_password_stripped = password_hash($password, PASSWORD_BCRYPT, $this->options);
        $final_password = $final_password_stripped;

        $sql = <<<EOT
            UPDATE sdgt_accounts SET password = '$final_password'
            WHERE pk = $pk;
EOT;
        return ClassParent::update($sql);
    }

    public function getUsers($data)
    {
        foreach ($data as $k => $v) {
            $data[$k] = pg_escape_string(trim(strip_tags($v)));
        }
        $status = $data['status'];

        $sql = <<<EOT
            SELECT
                pk,
                user_name,
                user_details
            FROM sdgt_accounts
            WHERE archived = '$status'
            ;
EOT;
        return ClassParent::get($sql);
    }
}
