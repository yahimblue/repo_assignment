app.controller('Login', function (
    $scope,
    SessionFactory,
    $window
) {
    $scope.isLoading = false;
    $scope.form = {
        user_name: '',
        password: ''
    }
    $scope.failedLoginCounter = null;

    init();

    function init() {
        $scope.isLoading = true;
        var promise = SessionFactory.checksession();
        promise.then(function (data) {
            $window.location = '#/upload'
            $scope.isLoading = false;
        }).catch(function (error) {
            $scope.isLoading = false;
            $window.location = '#/login';
        });
    }

    function failedLoginCounterGetter() {
        var promise = SessionFactory.failedLoginCounterGetter($scope.form);
        promise.then(function (data) {
            var counter = data.data.result[0];
            $scope.failedLoginCounter = counter.failed_login_counter;
        }).catch(function (error) {
            $scope.failedLoginCounter = 0;
        });
    }

    $scope.login = function () {
        if ($scope.form.user_name == '' || $scope.form.password == '') {
            swal({
                title: "Warning!",
                text: "Please enter your User Name or/and Password.",
                icon: "warning",
            });
        } else {
            $scope.isLoading = true;
            failedLoginCounterGetter();
            var promise = SessionFactory.login($scope.form)
            promise.then(function (data) {
                window.location.reload();
            }).catch(function (error) {
                $scope.isLoading = false;
                if ($scope.failedLoginCounter == 0) {
                    $scope.failedLoginCounter = 0;
                    getUserLogin();
                } else {
                    $scope.failedLoginCounter--;
                    getUserLogin();
                    swal({
                        title: "Error!",
                        text: `An error occured while loggin in. Please try again. You only have ${$scope.failedLoginCounter} left.`,
                        icon: "error",
                    });
                }
            })
        }
    }

    function getUserLogin() {
        var params = {
            user_name: $scope.form.user_name,
            failedCounter: $scope.failedLoginCounter
        };
        var promise = SessionFactory.getUserLogin(params)
        promise.then(function (data) {
            if ($scope.failedLoginCounter == 0) {
                swal({
                    title: "Error!",
                    text: "Account does not exist or you're account has been disabled for many login attemps!",
                    icon: "error",
                });
            }
        }).catch(function (error) {
            if ($scope.failedLoginCounter == 0) {
                swal({
                    title: "Error!",
                    text: "Account does not exist or you're account has been disabled for many login attemps!",
                    icon: "error",
                });
            }
        })
    }

});