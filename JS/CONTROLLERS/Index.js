app.controller('Index', function (
    $scope,
    SessionFactory,
    $window,
    md5
) {
    $scope.isLoggedIn = false;
    $scope.isLoading = false;
    $scope.pk = null;
    $scope.profile = {};

    init();

    setInterval(() => {
        var promise = SessionFactory.updatingAccountsStatus()
        promise.then(function () {
            // Reset failed_login_counter number
            console.log('rflcn');
        }).catch(function (error) {
            // No accounts to reset
            console.log('natr');
        })
    }, 60000);

    function init() {
        $scope.isLoading = true;
        var promise = SessionFactory.checksession();
        promise.then(function (data) {
            var _id = md5.createHash('pk');
            $scope.pk = data.data[_id];
            get_profile();
        }).catch(function (data) {
            $scope.isLoading = false;
            $scope.isLoggedIn = false;
            $window.location = '#/login';
        });
    }

    function get_profile() {
        $scope.isLoading = true;
        var parameters = {
            pk: $scope.pk
        }
        var promise = SessionFactory.get_profile(parameters)
        promise.then(function (data) {
            $scope.profile = data.data.result[0];
            $scope.profile.user_details = JSON.parse($scope.profile.user_details);
            console.log("TCL: functionget_profile -> $scope.profile", $scope.profile)
            $scope.isLoading = false;
            $scope.isLoggedIn = true;
        }).catch(function (error) {
            console.log("TCL: functionget_profile -> error", error)
            $scope.isLoading = false;
        })
    }

    $scope.log_out = function () {
        var parameters = {
            pk: $scope.profile.pk
        };

        var promise = SessionFactory.log_out(parameters);
        promise.then(function (data) {
            init();
        }).catch(function (error) {
            swal({
                title: "ERROR!",
                text: "An Error Occured! Please try again.",
                icon: "error"
            });
        });
    }

});