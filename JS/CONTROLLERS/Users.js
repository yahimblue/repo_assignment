app.controller('Users', function (
    $scope,
    SessionFactory,
    UsersFactory,
    $window,
    HistorylogsFactory,
    md5,
    ngDialog
) {
    $scope.isLoading = false;
    $scope.pk = null;
    $scope.filter = {};
    $scope.profile = {};
    $scope.useDefaultPassword = false;
    $scope.form = {
        user_name: '',
        job_position: '',
        full_name: '',
        final_password: '',
        userStatus: 'f'
    }
    $scope.password = {
        initial: '',
        repeated: ''
    }
    $scope.users = [];
    $scope.haveUsers = false;
    $scope.modal_form = {
        pk: '',
        user_name: '',
        job_position: '',
        full_name: '',
        final_password: ''
    }
    // Pagination Params Starts Here
    $scope.maxSize = 10;
    $scope.viewby = 5;
    $scope.currentPage = 1;
    $scope.itemsPerPage = $scope.viewby;
    // Pagination Params Ends Here

    init();

    function init() {
        $scope.isLoading = true;
        var promise = SessionFactory.checksession();
        promise.then(function (data) {
            var _id = md5.createHash('pk');
            $scope.pk = data.data[_id];
            get_profile();
            $scope.getUsers();
            $scope.isLoading = false;
        }).catch(function (data) {
            $scope.isLoading = false;
            $window.location = '#/login';
        });
    }

    $scope.getUsers = function () {
        var filter = {
            status: $scope.form.userStatus
        }
        var promise = UsersFactory.getUsers(filter)
        promise.then(function (data) {
            $scope.users = data.data.result;
            for (var i in $scope.users) {
                $scope.users[i].user_details = JSON.parse($scope.users[i].user_details);
            }
            $scope.TotalRegisterdUsers = $scope.users.length;
            $scope.haveUsers = true;
        }).catch(function (error) {
            $scope.haveUsers = false;
        })
    }

    // Pagination Changer
    $scope.pageChanged = function (currentPage) {
        $scope.currentPage = currentPage;
    };

    function get_profile() {
        $scope.isLoading = true;
        var parameters = {
            pk: $scope.pk
        }
        var promise = SessionFactory.get_profile(parameters)
        promise.then(function (data) {
            $scope.profile = data.data.result[0];
            $scope.profile.user_details = JSON.parse($scope.profile.user_details);
            $scope.isLoading = false;
        }).catch(function (error) {
            $scope.isLoading = false;
            $window.location = '#/login';
        })
    }

    $scope.undo = function () {
        for (var i in $scope.form) {
            $scope.form[i] = ''
        }
        for (var i in $scope.modal_form) {
            $scope.modal_form[i] = ''
        }
        $scope.form.userStatus = 'f'
        $scope.password.initial = ''
        $scope.password.repeated = ''
        $scope.useDefaultPassword = false
    }

    $scope.useDefault = function () {
        $scope.useDefaultPassword = $scope.useDefaultPassword == true ? false : true;
    }

    $scope.addUser = function () {
        if ($scope.useDefaultPassword) {
            $scope.form.final_password = 'default pass'
        } else {
            if ($scope.password.initial === $scope.password.repeated) {
                $scope.form.final_password = $scope.password.initial
            } else {
                swal({
                    title: "Warning!",
                    text: "Password/s does not match! Please try again.",
                    icon: "warning",
                });
            }
        }
        var error = 0;
        for (var i in $scope.form) {
            if ($scope.form[i] == '' || $scope.form[i] == null || $scope.form[i] == undefined) {
                error++;
            }
        }
        if (error > 0) {
            swal({
                title: "Warning!",
                text: "Please fill up all the forms!",
                icon: "warning",
            });
        } else {
            $scope.add_user = true;
            history_logs();
            $scope.isLoading = true;
            var promise = UsersFactory.addUser($scope.form)
            promise.then(function (data) {
                $scope.undo();
                $scope.isLoading = false
                swal({
                    title: "SUCCESS!",
                    text: "User successfuly added!",
                    icon: "success",
                });
                $scope.getUsers();
            }).catch(function (error) {
                $scope.isLoading = false
                swal({
                    title: "ERROR!",
                    text: "An error occured while adding a user! Please try again.",
                    icon: "error",
                });
            })
        }
    }

    $scope.updateUser = function (v) {
        $scope.modal_form.pk = v.pk;
        $scope.modal_form.user_name = v.user_name;
        $scope.modal_form.full_name = v.user_details.full_name;
        $scope.modal_form.job_position = v.user_details.position;
        ngDialog.openConfirm({
            template: 'UpdateUserModal',
            className: 'ngdialog-theme-plain',
            preCloseCallback: function (value) {
                var nestedConfirmDialog = ngDialog.openConfirm({
                    template: '<p>Are you sure you want to update this user?</p>' +
                        '<div class="ngdialog-buttons">' +
                        '<button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">No' +
                        '<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="confirm(1)">Yes' +
                        '</button></div>',
                    plain: true,
                    className: 'ngdialog-theme-plain'
                });
                return nestedConfirmDialog;
            },
            scope: $scope,
            showClose: false,
            closeByEscape: false,
            closeByNavigation: false,
            closeByDocument: false
        }).then(function (value) {
            return false;
        }, function (value) {
            $scope.edit_user = true;
            history_logs();
            $scope.isLoading = true;
            var promise = UsersFactory.updateUser($scope.modal_form)
            promise.then(function (data) {
                $scope.undo();
                $scope.isLoading = false
                swal({
                    title: "SUCCESS!",
                    text: "User details successfuly updated!",
                    icon: "success",
                });
                $scope.getUsers();
            }).catch(function (error) {
                $scope.isLoading = false
                swal({
                    title: "ERROR!",
                    text: "An error occured while updating user details! Please try again.",
                    icon: "error",
                });
            })
        })
    }

    $scope.updateUserAccountStatus = function (v) {
        $scope.modal_form.pk = v.pk;
        $scope.modal_form.status = $scope.form.userStatus == 'f' ? 't' : 'f';
        ngDialog.openConfirm({
            template: 'UpdateUserAccountStatusModal',
            className: 'ngdialog-theme-plain',
            scope: $scope,
            showClose: false,
            closeByEscape: false,
            closeByNavigation: false,
            closeByDocument: false
        }).then(function (value) {
            return false;
        }, function (value) {
            $scope.update_status_user = true;
            $scope.update_status_user_data = v.user_name;
            history_logs();
            $scope.isLoading = true;
            var promise = UsersFactory.updateUserAccountStatus($scope.modal_form)
            promise.then(function (data) {
                $scope.undo();
                $scope.isLoading = false
                swal({
                    title: "SUCCESS!",
                    text: "User account status has been successfully updated!",
                    icon: "success",
                });
                $scope.getUsers();
            }).catch(function (error) {
                $scope.isLoading = false
                swal({
                    title: "ERROR!",
                    text: "An error occured while updating user account status! Please try again.",
                    icon: "error",
                });
            })
        })
    }

    $scope.resetPassword = function (v) {
        $scope.modal_form.pk = v.pk;
        ngDialog.openConfirm({
            template: 'ResetUserPasswordModal',
            className: 'ngdialog-theme-plain',
            scope: $scope,
            showClose: false,
            closeByEscape: false,
            closeByNavigation: false,
            closeByDocument: false
        }).then(function (value) {
            return false;
        }, function (value) {
            $scope.reset_password_user = true;
            $scope.reset_password_user_data = v.user_name;
            history_logs();
            $scope.isLoading = true;
            var promise = UsersFactory.resetPassword($scope.modal_form)
            promise.then(function (data) {
                $scope.undo();
                $scope.isLoading = false
                swal({
                    title: "SUCCESS!",
                    text: "User account status has been successfully updated!",
                    icon: "success",
                });
                $scope.getUsers();
            }).catch(function (error) {
                $scope.isLoading = false
                swal({
                    title: "ERROR!",
                    text: "An error occured while updating user account status! Please try again.",
                    icon: "error",
                });
            })
        })
    }

    function history_logs() {

        if ($scope.add_user == true) {
            $scope.filter.committed_by_pk = $scope.profile.pk;
            $scope.filter.action = $scope.filter.committed_by + " added a user " + $scope.modal_form.user_name;
            $scope.filter.module_name = "Add Users";
        };

        if ($scope.edit_user == true) {
            $scope.filter.committed_by_pk = $scope.profile.pk;
            $scope.filter.action = $scope.filter.committed_by + " modified the user " + $scope.modal_form.user_name;
            $scope.filter.module_name = "Add Users";
        };

        if ($scope.update_status_user == true) {
            $scope.filter.committed_by_pk = $scope.profile.pk;
            $scope.filter.action = $scope.filter.committed_by + " updated the status of " + $scope.update_status_user_data;
            $scope.filter.module_name = "Add Users";
        };

        if ($scope.reset_password_user == true) {
            $scope.filter.committed_by_pk = $scope.profile.pk;
            $scope.filter.action = $scope.filter.committed_by + " resets the password of " + $scope.reset_password_user_data;
            $scope.filter.module_name = "Add Users";
        };

        var promise = HistorylogsFactory.save_history($scope.filter);
        promise.then(function (data) {

        }).catch(function (error) {

        });
    }

});