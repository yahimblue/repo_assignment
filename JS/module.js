var app = angular.module('onload', [
    'angular-md5',
    'angular-loading-bar',
    'ngAnimate',
    'angularFileUpload',
    'isteven-multi-select',
    'ui-notification',
    'ae-datetimepicker',
    'ngDialog',
    'ngRoute',
    'ui.bootstrap'
]);
app.config(function ($routeProvider) {
    $routeProvider
        .when('/login', {
            controller: 'Login',
            templateUrl: 'partials/Login/login-page.html'
        })
        .when('/reports', {
            controller: 'Reports',
            templateUrl: 'partials/Report/report.html'
        })
        .when('/inventory', {
            controller: 'Company',
            templateUrl: 'partials/Inventory/Inventory.html'
        })
        .when('/histinventory', {
            controller: 'Company',
            templateUrl: 'partials/Inventory/histinventory.html'
        })
        .when('/menu', {
            controller: 'Company',
            templateUrl: 'partials/Inventory/menu.html'
        })
        .when('/prodtransfer', {
            controller: 'Company',
            templateUrl: 'partials/Inventory/prodtransfer.html'
        })
        .when('/funds', {
            controller: 'Funds',
            templateUrl: 'partials/Funds/funds.html'
        })
        .when('/cashier', {
            controller: 'Cashier',
            templateUrl: 'partials/Cashier/cashier.html'
        })
        .when('/users', {
            controller: 'Users',
            templateUrl: 'partials/AddUsers/add-users-page.html'
        })
        .otherwise({
            redirectTo: '/cashier'
        })
});