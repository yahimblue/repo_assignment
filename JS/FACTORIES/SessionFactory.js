app.factory('SessionFactory', function ($http) {
    var factory = {};

    factory.checksession = function (data) {
        var promise = $http({
            url: './FUNCTIONS/Session/getsession.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            transformRequest: function (obj) {
                var str = [];
                for (var p in obj)
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            },
            data: data
        })

        return promise;
    };

    factory.login = function (data) {
        var promise = $http({
            url: './FUNCTIONS/Session/login.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            transformRequest: function (obj) {
                var str = [];
                for (var p in obj)
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            },
            data: data
        })

        return promise;
    };

    factory.log_out = function (data) {
        var promise = $http({
            url: './FUNCTIONS/Session/deletesession.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            transformRequest: function (obj) {
                var str = [];
                for (var p in obj)
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            },
            data: data
        })

        return promise;
    };

    factory.get_profile = function (data) {
        var promise = $http({
            url: './FUNCTIONS/Session/get_profile.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            transformRequest: function (obj) {
                var str = [];
                for (var p in obj)
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            },
            data: data
        })

        return promise;
    };

    factory.logOutUserIfPasswordHasbeenReset = function (data) {
        var promise = $http({
            url: './FUNCTIONS/Session/logOutUserIfPasswordHasbeenReset.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            transformRequest: function (obj) {
                var str = [];
                for (var p in obj)
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            },
            data: data
        })

        return promise;
    };

    factory.updatePasswordResetStatus = function (data) {
        var promise = $http({
            url: './FUNCTIONS/Session/updatePasswordResetStatus.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            transformRequest: function (obj) {
                var str = [];
                for (var p in obj)
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            },
            data: data
        })

        return promise;
    };

    factory.getUserLogin = function (data) {
        var promise = $http({
            url: './FUNCTIONS/Session/getUserLogin.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            transformRequest: function (obj) {
                var str = [];
                for (var p in obj)
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            },
            data: data
        })

        return promise;
    };

    factory.failedLoginCounterGetter = function (data) {
        var promise = $http({
            url: './FUNCTIONS/Session/failedLoginCounterGetter.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            transformRequest: function (obj) {
                var str = [];
                for (var p in obj)
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            },
            data: data
        })

        return promise;
    };

    factory.updatingAccountsStatus = function (data) {
        var promise = $http({
            url: './FUNCTIONS/Session/updatingAccountsStatus.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            transformRequest: function (obj) {
                var str = [];
                for (var p in obj)
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            },
            data: data
        })

        return promise;
    };

    return factory;
});