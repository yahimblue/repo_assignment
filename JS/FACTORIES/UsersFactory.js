app.factory('UsersFactory', function ($http) {
    var factory = {};

    factory.addUser = function (data) {
        var promise = $http({
            url: './FUNCTIONS/Users/addUser.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            transformRequest: function (obj) {
                var str = [];
                for (var p in obj)
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            },
            data: data
        })
        return promise;
    };

    factory.getUsers = function (data) {
        var promise = $http({
            url: './FUNCTIONS/Users/getUsers.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            transformRequest: function (obj) {
                var str = [];
                for (var p in obj)
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            },
            data: data
        })

        return promise;
    };

    factory.updateUser = function (data) {
        var promise = $http({
            url: './FUNCTIONS/Users/updateUser.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            transformRequest: function (obj) {
                var str = [];
                for (var p in obj)
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            },
            data: data
        })
        return promise;
    };

    factory.updateUserAccountStatus = function (data) {
        var promise = $http({
            url: './FUNCTIONS/Users/updateUserAccountStatus.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            transformRequest: function (obj) {
                var str = [];
                for (var p in obj)
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            },
            data: data
        })
        return promise;
    };

    factory.resetPassword = function (data) {
        var promise = $http({
            url: './FUNCTIONS/Users/resetPassword.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            transformRequest: function (obj) {
                var str = [];
                for (var p in obj)
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            },
            data: data
        })
        return promise;
    };

    return factory;
});